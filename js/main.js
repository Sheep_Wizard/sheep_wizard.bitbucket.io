//classes
/*
Frame class will contains the cell which hold the images for the collage
*/
class Frame{
    constructor(){
        this.width = 800;
        this.height = 800;
        this.colour = {
            r: 255,
            g: 255,
            b: 255
        };
        this.position = {
            x: 0,
            y: 0,
            width: 0,
            height: 0
        }
        this.mouseOver = false;
    }

    mouseMoved() {
        this.isMouseOver();
    }

    isMouseOver() {
        if (mouseFlags.mousePos.x > this.position.x && mouseFlags.mousePos.x < this.position.x + this.position.width && mouseFlags.mousePos.y > this.position.y && mouseFlags.mousePos.y < this.position.y + this.position.height) {
            this.mouseOver = true;
            return true;
        }
        else {
            this.mouseOver = false;
            return false;
        }
    }

    isClicked() {
        if (this.isMouseOver()) {
            console.log("click");
            const elm = document.querySelector(".listImagesBorder")
            if(elm != undefined){
                const x = (mouseFlags.mousePos.x - frame.position.x);
                const y = (mouseFlags.mousePos.y - frame.position.y);
                new Sticker(frame, () =>{
                    return{
                        x: frame.position.x + applyZoom(x),
                        y: frame.position.y + applyZoom(y),
                    }
                }, elm.src);
            }
        }
    }

    drawFrame() {   
        this.setPosition();
        var x = this.position.x;
        var y = this.position.y;
        var width = applyZoom(this.width);
        var height = applyZoom(this.height);
        ctx.shadowBlur = 20;
        ctx.shadowColor = "black";
        ctx.fillStyle = "rgb("+this.colour.r+","+this.colour.g+","+this.colour.b+")";
        ctx.fillRect(x, y, width, height);
        ctx.shadowBlur = 0;
    }

    setPosition(){
        const middle = getCanvasCenter();
        this.position = {
            x: middle.x - (applyZoom(this.width)/2),
            y: middle.y - (applyZoom(this.height)/2),
            width: applyZoom(this.width),
            height: applyZoom(this.height)
        };
    }   
}

class CanvasButton{
    constructor(frame, position){
        this.frame = frame;
        this.position = position;
        this.x = 0;
        this.y = 0;
        this.width = 0;
        this.height = 0;
        this.mouseOver = false;
        this.colour = {
            r: 0,
            g: 0,
            b: 0,
            a: 1
        };
        this.circle = false;
        this.clicked = () =>{}
        canvasButtonList.push(this);
    }

    draw(){
        this.x = this.position().x;
        this.y = this.position().y;
        this.width = this.position().width;
        this.height = this.position().height;

        ctx.fillStyle = "rgba("+this.colour.r+","+this.colour.g+","+this.colour.b+", 1)";
        if(this.circle){
            ctx.beginPath();
            ctx.arc(this.x + (this.width/2), this.y + (this.width/2) , this.width, 0, 2 * Math.PI);
            ctx.closePath();
            ctx.fill();
        }
        else{
            ctx.fillRect(this.x, this.y, this.width, this.height);
        }
    }

    mouseMoved(){
        this.isMouseOver();
    }

    isMouseOver(){
        if (mouseFlags.mousePos.x > this.x && mouseFlags.mousePos.x < this.x + this.width && mouseFlags.mousePos.y > this.y && mouseFlags.mousePos.y < this.y + this.height){
            this.mouseOver = true;
            return true;
        }
        else{
            this.mouseOver = false;
            return false;
        }   
    }

    isClicked(){
        if (this.isMouseOver()){
            this.clicked();
        }
    }
}

//Resize bars for resizing the frame
class ResizeBar extends CanvasButton{
    constructor(frame, position, type) {
        super(frame, position);
        this.reSizetype = type;
        this.hidden = true;
        this.opacity = 0.8;
        this.colour = {
            r: 17,
            g: 17,
            b: 17,
            a: 0.8
        };
        this.dragged = false;
        reSizeBarList.push(this);
    }

    draw() {
        if (mouseFlags.mouseEntered){   
            this.x = this.position().x;
            this.y = this.position().y;
            this.width = this.position().width;
            this.height = this.position().height;

            if(this.mouseOver || this.dragged){
                ctx.fillStyle = "rgba(" + this.colour.r + "," + this.colour.g + "," + this.colour.b + "," + 1 + ")";
            }
            else{
                ctx.fillStyle = "rgba(" + this.colour.r + "," + this.colour.g + "," + this.colour.b + "," + this.colour.a + ")";
            }
            
            ctx.fillRect(this.x, this.y, this.width, this.height);

        }  
    }

    mouseMoved() {
        this.isMouseOver();
        if(mouseFlags.mousePressed && this.mouseOver){
            for (var re of reSizeBarList){
                if(re.dragged){
                    return;
                }
            }
            this.dragged = true;
            
        }
        if(!mouseFlags.mousePressed){
            this.dragged = false;
        }
        if(this.dragged){
            const minSize = 40;
            const x = mouseFlags.mousePos.x;
            const y = mouseFlags.mousePos.y;
            if (this.reSizetype == "left"){           
                if(x > this.frame.position.x){
                    this.frame.width -= Math.ceil(x - this.frame.position.x);
                }
                else{
                    this.frame.width += Math.ceil(this.frame.position.x - x);
                }
            }
            else if(this.reSizetype == "right"){
                if (x > this.frame.position.x + this.frame.position.width){
                    this.frame.width += Math.ceil(x - (this.frame.position.x + this.frame.position.width))
                }
                else{
                    this.frame.width -= Math.ceil((this.frame.position.x + this.frame.position.width) - x);
                }
            }
            else if(this.reSizetype == "top"){
                if(y > this.frame.position.y){
                    this.frame.height -= Math.ceil(y - this.frame.position.y);
                }
                else{
                    this.frame.height += Math.ceil(this.frame.position.y - y);
                }
            }
            else if(this.reSizetype == "bottom"){
                if(y > this.frame.position.y + this.frame.position.height){
                    this.frame.height += Math.ceil(y - (this.frame.position.y + this.frame.position.height));
                }
                else{
                    this.frame.height -= Math.ceil((this.frame.position.y + this.frame.position.height) - y);
                }
            }
            if (this.frame.width < minSize) {
                this.frame.width = minSize;
            }
            if(this.frame.height < minSize){
                this.frame.height = minSize;
            }
        }
    }
}

class Sticker extends CanvasButton{
    constructor(frame, position, img){
        super(frame, position);
        this.img = img;
        this.offsetX = 0;
        this.offsetY = 0;
        this.width = 300;
        this.height = 300;
        stickerList.push(this);
    }

    draw(){
        this.x = this.position().x;
        this.y = this.position().y;

        const image = new Image();
        image.src = this.img;

        ctx.drawImage(image, this.x, this.y, applyZoom(this.width), applyZoom(this.height));
    }
}


/*Global variables */
var canvas;
var ctx;
var frame;
var zoom = 50;
const sideBarSize = {
    x: 360 + 5,
    y: 50 + 5
}
const mouseFlags = {
    mouseEntered: false,
    mousePressed: false,
    mousePos: {
        x: 0,
        y: 0
    }
}
const canvasControls = {
    x: 0,
    y: 0
}
const canvasButtonList = [];
const reSizeBarList = [];
const stickerList = [];
const images = [];

window.onload = () =>{

    //Initialize canvas
    const cvn = document.getElementById("canvas");
    if(cvn != undefined){
        canvas = cvn;
        canvas.width = window.innerWidth - sideBarSize.x;
        canvas.height = window.innerHeight - sideBarSize.y;
        canvas.addEventListener("click", canvasClick);
        canvas.addEventListener("mousedown", canvasMouseDown);
        canvas.addEventListener("mouseup", canvasMouseUp);
        canvas.addEventListener("keydown", canvasKeyDown);
        canvas.addEventListener("keyup", canvasKeyUp);
        canvas.addEventListener("mouseenter", canvasMouseEnter);
        canvas.addEventListener("mouseleave", canvasMouseLeave);
        canvas.addEventListener("mousemove", canvasMouseMove);
        canvas.addEventListener('contextmenu', canvasContextMenu);
        if (canvas.getContext) {
            ctx = canvas.getContext("2d");
            frame = new Frame();

            leftResizeBar = new ResizeBar(frame, () => {
                return {
                    x: frame.position.x - applyZoom(30),
                    y: frame.position.y + applyZoom(5),
                    width: applyZoom(10),
                    height: frame.position.height - applyZoom(10)
                }
            }, "left")
            rightResizeBar = new ResizeBar(frame, () =>{
                return{
                    x: frame.position.x + frame.position.width + applyZoom(20),
                    y: frame.position.y + applyZoom(5),
                    width: applyZoom(10),
                    height: frame.position.height - applyZoom(10)
                }
            }, "right")
            topResizeBar = new ResizeBar(frame, () =>{
                return{
                    x: frame.position.x + applyZoom(5),
                    y: frame.position.y - applyZoom(30),
                    width: frame.position.width - applyZoom(10),
                    height: applyZoom(10)
                }
            }, "top")
            buttomResizeBar = new ResizeBar(frame, () =>{
                return{
                    x: frame.position.x + applyZoom(5),
                    y: frame.position.y + frame.position.height + applyZoom(20),
                    width: frame.position.width - applyZoom(10),
                    height: applyZoom(10)
                }
            }, "bottom")        
            canvasDraw();
        }
        else {
            console.log("No canvas support");
        }
    
    }

    //event listeners
    const slider = document.getElementById("zoomslider")
    const sliderNum = document.getElementById("zoomnumber");
    sliderNum.disabled = true;
    slider.oninput = function(){
        zoom = this.value;
        sliderNum.value = zoom;
        canvasDraw();
    }

    document.getElementById("uploadbackground").addEventListener("change", uploadImages);
}
window.onresize = () =>{
    canvas.width = window.innerWidth - sideBarSize.x;
    canvas.height = window.innerHeight - sideBarSize.y;
    canvasDraw();
}

/*Canvas related functions*/
/*Canvas event listeners*/
canvasClick = (event) =>{
    frame.isClicked();
    for (var btn of canvasButtonList) {
        btn.isClicked();
    }
    canvasDraw();
}
canvasMouseDown = (event) => {
    mouseFlags.mousePressed = true;
}
canvasMouseUp = (event) => {
    mouseFlags.mousePressed = false;
}
canvasKeyDown = (event) => {
   
}
canvasKeyUp = (event) => {
}
canvasMouseEnter = (event) => {
    mouseFlags.mouseEntered = true;
}
canvasMouseLeave = (event) => {
    mouseFlags.mouseEntered = false;

    canvasDraw();
}
canvasMouseMove = (event) => {
    mouseFlags.mousePos.x = event.clientX - canvas.getBoundingClientRect().left;
    mouseFlags.mousePos.y = event.clientY - canvas.getBoundingClientRect().top;
    frame.mouseMoved();
    for (var btn of canvasButtonList) {
        btn.mouseMoved();
    }
    canvasDraw();
}
canvasContextMenu = (event) =>{
    event.preventDefault();
    drawContextMenu();
    return false;
}

canvasDraw = () => {
    //draw backcolour
    ctx.fillStyle = "rgb(70,70,70)";
    ctx.fillRect(0, 0, canvas.width, canvas.height);
    frame.drawFrame();

    for (var btn of canvasButtonList) {
        btn.draw();
    }
}

drawContextMenu = () =>{
    const x = mouseFlags.mousePos.x;
    const y = mouseFlags.mousePos.y;

    ctx.fillStyle = "rgb(70,70,70)";
    ctx.fillRect(x,y,100,100);
}



//Image uploader
uploadImages = (event) =>{
    var files = event.target.files;
    for (var i = 0, f; f = files[i]; i++) {
        if (!f.type.match('image.*')) {
            continue;
        }
        var reader = new FileReader();
        reader.onload = (event) =>{
            images.push(event.target.result);
            
            updateImageList(images[images.length-1]);
        }
        reader.readAsDataURL(f);
    }
}

/*
sticker = new Sticker(frame, () => {
                const x = frame.position.x + this.offsetX;
                const y = frame.position.y + this.offsetY;
                return {
                    x: x,
                    y: y,
                    width: 300,
                    height: 300,
                }
            }, event.target.result)*/

updateImageList = (image) =>{
    const list = document.getElementById("imageList");

    const li = document.createElement("li");
    const img = document.createElement("img");
    img.src = image;
    img.className = "listImages";
    img.addEventListener("click", (event) =>{
        const elms = document.querySelectorAll(".listImagesBorder");
        for(var i = 0; i<elms.length; i++){
            elms[i].classList.remove("listImagesBorder");
            elms[i].className = "listImages";
        }
        event.target.className = "listImagesBorder";
    })
    if(img.selected != undefined){
        if(img.selected == true){
            img.style.border = "4px solid white";
        }
    }
    li.appendChild(img);
    list.appendChild(li);
}



/* Utility functions */
//Get the center point of the canvas
//returns {x,y}
getCanvasCenter = () =>{
    return {
        x: canvas.width/2,
        y: canvas.height/2
    };
}
//apply zoom value to canvas drawing
applyZoom = (num) =>{
    return num*(zoom/50);
}